package conifg;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.testng.annotations.BeforeTest;

import io.restassured.path.json.JsonPath;

import junit.framework.Assert;


public class Test {
	String url;
	String command[]=new String[10];
	String key;
	int status[]=new int [10];
	String var1[]=new String [10];
	String var2[]=new String [10];
	String var3[]=new String [10];
	String var4[]=new String [10];
	String var5[]=new String [10];
	String var6[]=new String [10];
	int var7[]=new int [10];
	int var8[]=new int [10];
	int rowStart,rowEnd;
    @BeforeTest
	public void test() throws IOException, NoSuchAlgorithmException
	{
	  ReadExcel read=new ReadExcel();
	   rowStart=read.getRowStart();
	  rowEnd=read.getRowEnd();
	  url=read.getUrl();
	  key=read.getKey();
	  command=read.getCommand();
	  var1=read.getVar(1);
	  var2=read.getVar(2);
	  var3=read.getVar(3);
	  var4=read.getVar(4);
	  var5=read.getVar(5);
	  var6=read.getVar(6);
	  var7=read.getIntVar(7);
	  var8=read.getIntVar(8);
	  status=read.getStatus();
	  

	  
	}
    @org.testng.annotations.Test(priority=0,description="This test case test whether the card is saved or not")
    void verifySave() throws NoSuchAlgorithmException
    {
    
    	Pair ans=Excel.decide(url, command[1], key, var1[1], var2[1], var3[1], var4[1], var5[1], var6[1], var7[1], var8[1]);
    	JsonPath result=ans.response.jsonPath();
    	
    	
    	int actual=result.get("status");
    	String msg=result.get("msg");
    	System.out.println(msg);
    	 Assert.assertEquals(status[1], actual);
    	
    	}
    
@org.testng.annotations.Test(priority=1,description="This test case test whether the  saved card is fetched or not")
void verifyGet() throws NoSuchAlgorithmException
{

	Pair ans=Excel.decide(url, command[2], key, var1[2], var2[2], var3[2], var4[2], var5[2], var6[2], var7[2], var8[2]);
	JsonPath result=ans.response.jsonPath();
	int actual=result.get("status");
	String msg=result.get("msg");
	System.out.println(msg);

	 Assert.assertEquals(status[2], actual);
	
	}

@org.testng.annotations.Test(priority=2,description="This test case test whether the  saved card is deleted or not")
void verifyDelete() throws NoSuchAlgorithmException
{

	Pair ans=Excel.decide(url, command[3], key, var1[3], var2[3], var3[3], var4[3], var5[3], var6[3], var7[3], var8[3]);
	JsonPath result=ans.response.jsonPath();
	int actual=result.get("status");
	
	
	String msg=result.get("msg");
	System.out.println(msg);
	 Assert.assertEquals(status[3], actual);
	
	}

    @org.testng.annotations.Test
    void verifyHeader() throws NoSuchAlgorithmException
    {
    	for(int i=rowStart+1;i<=rowEnd;++i)
    	{
    		
    		Pair ans=Excel.decide(url, command[i], key, var1[i], var2[i], var3[i], var4[i], var5[i], var6[i], var7[i], var8[i]);
    		String contentType = ans.response.header("Content-Type");
    		Assert.assertEquals("text/html; charset=UTF-8", contentType);
    		String serverType =  ans.response.header("Server");
    			
    		Assert.assertEquals("Apache",serverType);
   

    		
    	
    	}
    	
    		
    }
    @org.testng.annotations.Test(priority=3,description="This test case test whether the  deleted card can be fetched or not")
    void verifyGet_deleted_card() throws NoSuchAlgorithmException
    {

    	Pair ans=Excel.decide(url, command[4], key, var1[4], var2[4], var3[4], var4[4], var5[4], var6[4], var7[4], var8[4]);
    	JsonPath result=ans.response.jsonPath();
    	int actual=result.get("status");
    	
    	
    	String msg=result.get("msg");
    	System.out.println(msg); 
    	 Assert.assertEquals(status[4], actual);
    	
    	}
    @org.testng.annotations.Test(priority=4,description="This test case test whether the  deleted card can be deleted or not")
    void verifyDelete_theDeleted() throws NoSuchAlgorithmException
    {

    	Pair ans=Excel.decide(url, command[5], key, var1[5], var2[5], var3[5], var4[5], var5[5], var6[5], var7[5], var8[5]);
    	JsonPath result=ans.response.jsonPath();
    	int actual=result.get("status");
    	
    	
    	String msg=result.get("msg");
    	System.out.println(msg); 
    	 Assert.assertEquals(status[5], actual);
    	
    	}
    @org.testng.annotations.Test(priority=5,description="This test case test whether a wrong  card can be saved or not")
    void verifySave_wrong_card() throws NoSuchAlgorithmException
    {

    	Pair ans=Excel.decide(url, command[6], key, var1[6], var2[6], var3[6], var4[6], var5[6], var6[6], var7[6], var8[6]);
    	JsonPath result=ans.response.jsonPath();
    	int actual=result.get("status");
    	
    	String msg=result.get("msg");
    	System.out.println(msg); 

    	 Assert.assertEquals(status[6], actual);
    	
    	}
   
}

