package conifg;

import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;


import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;


public class Get_Card {

public static Pair Get_card(String url,String command,String key,String var1) throws NoSuchAlgorithmException
{
String input="smsplus|"+command+"|"+var1+"|1b1b0";
	

	String hash=helper.hashGenerator(input);
	RestAssured.baseURI=url;
	Response response=(Response)  RestAssured.given()
	.param("form", "2")
	.param("key", key)
    .param("command",command)
     .param("hash", hash)
	.param("var1",var1).post()
	.then()
	.statusCode(200).extract().response();
	
   JsonPath json=response.jsonPath();
   String msg=json.getString("msg");
   String token="";
   Pair ans=new Pair();
   if(msg.equals("Card not found."))
	     {
	       token="No token";
	        ans.token=token;
	        ans.response=response;
	         return ans;
	        
	     }
   
   Map<String, Map<String, String>> val = new LinkedHashMap<String, Map<String, String>>();
	val = json.get("user_cards");
	Map.Entry<String, Map<String, String>> entry = val.entrySet().iterator().next();
	token = entry.getKey();
  ans.response=response;
   ans.token=token;
   
	  

return ans;
	
}

}
